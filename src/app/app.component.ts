import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'qs-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  constructor(private _iconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer,
    private _translate: TranslateService) {
    this._iconRegistry.addSvgIconInNamespace('assets', 'company',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/company.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'github',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/github.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'logo',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/logo.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'logo-mark',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/logo-mark.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'company-ux',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/company-ux.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'appcenter',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/appcenter.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'listener',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/listener.svg'));
    this._iconRegistry.addSvgIconInNamespace('assets', 'querygrid',
      this._domSanitizer.bypassSecurityTrustResourceUrl('assets/icons/querygrid.svg'));

    // this language will be used as a fallback when a translation isn't found in the current language
    _translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
    _translate.use('en');
  }

}
